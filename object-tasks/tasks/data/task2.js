export default [
  // case 1
  [
    [
      { id: 1, name: 'Michael' },
      { id: 1, age: 30 },
    ],
    { name: 'Michael', age: 30 },
  ],
  // case 2
  [
    [
      { a: 1, b: 2 },
      { a: 2, c: 4 },
      { a: 3, d: 9 },
    ],
    { b: 2, c: 4, d: 9 },
  ],
  // case 3
  [
    [
      { a: null, b: 1 },
      { b: null, a: 1 },
    ],
    {},
  ],
  // case 4
  [
    [
      { f: [1, 2, 3], [1]: [1] },
      { f: {}, [2]: [2] },
    ],
    { 1: [1], 2: [2] },
  ],
  // case 5
  [
    [
      { a: 1 },
      { b: 1 },
      { c: 1 },
      { d: 1 },
      { f: 1 },
    ],
    { a: 1, b: 1, c: 1, d: 1, f: 1 },
  ],
  // case 6
  [
    [],
    {},
  ],
  // case 7
  [
    [{ a: null, b: undefined, c: NaN }],
    { a: null, b: undefined, c: NaN },
  ],
  // case 8
  [
    [
      { [[]]: 1 },
      { [[]]: 2 },
      { [[]]: 3 },
    ],
    {},
  ],
  // case 9
  [
    [
      { [{ a: 1 }]: false },
      { [{ b: 1 }]: false },
      { [{ c: 1 }]: true },
      { d: false },
    ],
    { d: false },
  ],
  // case 10
  [
    [
      { null: null },
      { undefined: undefined },
      { false: false },
      { NaN: NaN },
    ],
    {
      null: null,
      undefined: undefined,
      false: false,
      NaN: NaN,
    },
  ],
];
