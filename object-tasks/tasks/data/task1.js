export default [
  // case 1
  [
    {
      id: 1,
      name: 'John',
    },
    {
      id: 1,
      name: 'John',
    },
  ],
  // case 2
  [
    { a: 1, b: 2, c: 3, d: 4, e: '5', f: null },
    { a: 1, b: 2, c: 3, d: 4, e: '5', f: null },
  ],
  // case 3
  [
    { grades: [1, 2, 3] },
    { grades: [1, 2, 3] },
  ],
  // case 4
  [
    { '1': [2], '2': [3], '3': 1 },
    { '1': [2], '2': [3], '3': 1 },
  ],
  // case 5
  [
    {},
    {},
  ],
  // case 6
  [
    { 1: undefined },
    { 1: undefined },
  ],
  // case 7
  [
    { [[1, 2]]: null },
    { [[1, 2]]: null },
  ],
  // case 8
  [
    { [{}]: {} },
    { [{}]: {} },
  ],
  // case 9
  [
    { [{}]: [{}] },
    { [{}]: [{}] },
  ],
  // case 10
  [
    { ...[1, 2, 3] },
    { ...[1, 2, 3] },
  ],
];
