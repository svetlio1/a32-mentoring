// Graduates
export default function (data) {

  const { course, minPassingGrade, students } = data;
  const graduates = [];

  students.forEach((student) => {
    const averageGrade = +student.grades.reduce((acc, grade) => {
      return acc + (grade / student.grades?.length);
    }, 0).toFixed(1);

    if (averageGrade >= minPassingGrade) {
      graduates.push({ name: student.name, score: averageGrade });
    }
  });

  const courseAverage = +graduates.reduce((acc, el) => {
    return acc + el.score / graduates.length;
  }, 0).toFixed(1);

  return {
    course,
    minPassingGrade,
    courseAverage,
    graduates
  }

};
