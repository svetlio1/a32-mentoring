// Unique object properties
export default function (data) {

  const result = {};
  const allKeys = [];

  data.forEach((obj) => {
    for (const key in obj) {
      if (allKeys.includes(key)) {
        delete result[key];
      } else {
        result[key] = obj[key];
      }
      allKeys.push(key);
    }

  });
  console.log(result);

  return result;
};

const uniqueKeys = function (data) {

  const result = {};
  const allKeys = [];

  data.forEach((obj) => {
    for (const key in obj) {
      if (allKeys.includes(key)) {
        delete result[key];
      } else {
        result[key] = obj[key];
      }
      allKeys.push(key);
    }

  });

  console.log(result);
  return result;
};
