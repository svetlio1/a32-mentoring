// Clone objects
export default function (data) {

  const cloned = {};
  for (const key in data) {
    if (Array.isArray(data[key])) {
      cloned[key] = data[key].slice();
    } else {
      cloned[key] = data[key];
    }

  }

  return cloned;
};