import modifiers from '../src/utils/modifiers.js';
import { rules } from '../src/utils/rules.js';
import data1 from './data/task1.js';
import data2 from './data/task2.js';
import data3 from './data/task3.js';
import solution1 from './solutions/task1.js';
import solution2 from './solutions/task2.js';
import solution3 from './solutions/task3.js';

export default [
  {
    name: 'Clone objects',
    data: data1,
    solution: solution1,
    modifiers: [modifiers.noClone],
    rules: [rules.immutableData],
  },
  {
    name: 'Unique object properties',
    data: data2,
    solution: solution2,
    modifiers: [modifiers.noClone],
    rules: [rules.immutableData],
  },
  {
    name: 'Graduates',
    data: data3,
    solution: solution3,
    modifiers: [modifiers.noClone],
    rules: [rules.immutableData],
  },
];
