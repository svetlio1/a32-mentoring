import { RulesetError } from './ruleset-error.js';

export function eq(result, test, noClone = false) {

  if (result === null && test === null) return true;

  if (Number.isNaN(result) && Number.isNaN(test)) return true;

  if (typeof test !== 'object') return result === test;

  if (Array.isArray(test)) {
    if (!Array.isArray(result)) return false;

    if (noClone) {
      return test.length === result.length && test.every((value, index) => eq(result[index], value, noClone));
    }

    return result === test;
  }

  if (result === test && !noClone) return true;

  return Object
    .keys(test)
    .every(key => eq(result[key], test[key], noClone));

};

const freezeHandler = {
  set() {
    throw new RulesetError('You are now allowed to modify passed objects!');
  },
};

export function freezeObject(o) {
  if (Array.isArray(o)) {
    return new Proxy(o.map(x => freezeObject(x)), freezeHandler);
  }
  if (o && typeof o === 'object') {
    Object.keys(o).every(key => o[key] = freezeObject(o[key]))

    return new Proxy(o, freezeHandler);
  }

  return o;
}
