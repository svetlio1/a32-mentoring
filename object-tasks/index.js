import colors from './src/utils/colors.js';
import modifiers from './src/utils/modifiers.js';
import { eq } from './src/utils/object-utils.js';
import { applyRules, liftRules } from './src/utils/rules.js';
import { RulesetError } from './src/utils/ruleset-error.js';
import tasks from './tasks/index.js';

(async () => {

  for (const task of tasks) {
    const maxCases = task.data.length;
    let cases = 0;

    console.log(`\n\n${colors.taskTitle}> ${task.name}:${colors.reset}`);

    for (const [input, output] of task.data) {
      const inputData = applyRules(task.rules, input);

      try {
        const result = await task.solution(inputData);

        if (eq(
          result,
          output,
          task.modifiers.includes(modifiers.noClone),
        )) {
          console.log(`${colors.green}  ✔ Success!${colors.reset}`);
          cases++;
        } else {
          console.log(`${colors.red}  ✘ Wrong answer!${colors.reset}`);
        }
      } catch (e) {
        if (e instanceof RulesetError) {
          console.log(`${colors.red}  ✘ ${colors.reset}${e.message}`);
        } else {
          console.log(`${colors.red}  ✘ Runtime error!${colors.reset}`);
        }
      }

      liftRules(task.rules);
    }

    const points = Math.ceil(100 * cases / maxCases);

    if (points === 100) {
      console.log(`${colors.green}100 points ${cases} / ${maxCases} cases`);
    } else {
      console.log(`${colors.yellow}${points} points ${cases} / ${maxCases} cases`);
    }
  }

})()
  .catch((e) => console.error(`Something went wrong...${e.message}`));